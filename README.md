Une carte représentant la Tente des Indécis, accompagnée d'aides de jeu et d'intérêts scénaristiques, tournant notamment autour du personnage de l'Indécis.

Toutes les images, hors des maps, viennent de Pixabay.com.
